# -------------------------- PRUEBA RUBY ----------------------------- #
file = File.open('alumnos.csv', 'r')
data = file.readlines.map(&:chomp)
data_array = []
data.each { |e| data_array.push(e.split(', ')) }
data_hash = {}
data_array.each { |e| data_hash[e[0].to_sym] = e[1..5].map(&:to_i) }
puts 'Alumnos:'
puts data_hash

option = 0
while option != 4
  # ---------------------- Main menu -------------------------- #
  puts
  puts '|-------------------------- Menu ------------------------------|'
  puts "Escoge una opcion:\n"
  puts 'Opcion 1: Generar archivo con nombre y promedio de cada alumno.-'
  puts 'Opcion 2: Ver inasistencias totales.-'
  puts 'Opcion 3: Alumnos aprobados.-'
  puts 'Opcion 4: Salir.-'
  puts

  option = 0
  while option <= 0 || option > 4
    puts 'Ingrese la opción:'
    option = gets.chomp.to_i
    puts
  end

  # ---------------------- Option 1 -------------------------- #
  def generate_file(students) # Generar Archivo
    averages = {}
    students.each { |key, value| averages[key] = value.sum / value.length.to_f }
    puts 'Promedios:'
    puts averages

    data = ''
    averages.each { |key, value| data << "#{key}, #{value}\n" }
    File.open('averages.csv', 'w') { |file| file.puts data }
    puts 'Archivo generado...'    
  end

  # ---------------------- Option 2 -------------------------- #
  def total_absences(students) # Total inasistencias
    absents = {}
    students.each { |key, value| absents[key] = value.count(0) }
    puts 'Inasistencias de cada alumno:'
    puts absents

    total = 0
    absents.each_value { |value| total += value }
    puts "\nTotal de inasistencias: #{total}"
  end
  # ---------------------- Option 3 -------------------------- #
  def approved_students(students, score) # Alumnos aprobados
    averages = {}
    students.each { |key, value| averages[key] = value.sum / value.length.to_f }
    puts 'Alumnos aprobados:'
    averages.each { |key, value| puts "#{key} = #{value}" if value > score }
  end

  # ---------------------- Option 4 -------------------------- #
  def exit_program
    puts 'Saliendo...'
  end

  # -------------------- Ejecutar opciones ------------------------- #
  case option
  when 1 then generate_file(data_hash)
  when 2 then total_absences(data_hash)
  when 3 then approved_students(data_hash, 5)
  when 4 then exit_program
  end
end